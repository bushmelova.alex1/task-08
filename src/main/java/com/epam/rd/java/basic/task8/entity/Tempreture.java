package com.epam.rd.java.basic.task8.entity;

/**
 * simple entity of the xml file
 */
public class Tempreture {
    private Integer tempreture;

    public Integer getTempreture() {
        return tempreture;
    }

    public void setTempreture(Integer tempreture) {
        this.tempreture = tempreture;
    }

    @Override
    public String toString() {
        return tempreture.toString();
    }
}

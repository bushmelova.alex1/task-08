package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;

import java.util.Comparator;

/**
 * util class to sort values by different fields
 */
public final class Sort {

    public static final Comparator<Flower> SORT_ONE = Comparator.comparing(Flower::getName);
    public static final Comparator<Flower> SORT_TWO =
            Comparator.comparing(element -> element.getVisualParameters().getAveLenFlower().getAveLenFlower());
    public static final Comparator<Flower> SORT_THREE =
            Comparator.comparing(element -> element.getGrowingTips().getTempreture().getTempreture());

    public static void sortCaseOne(Flowers flowers) {
        flowers.getFlowers().sort(SORT_ONE);
    }

    public static Flowers sortCaseTwo(Flowers flowers) {
        flowers.getFlowers().sort(SORT_TWO);
        return flowers;
    }

    public static Flowers sortCaseThree(Flowers flowers) {
        flowers.getFlowers().sort(SORT_THREE);
        return flowers;
    }
}

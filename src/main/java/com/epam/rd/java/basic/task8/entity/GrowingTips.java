package com.epam.rd.java.basic.task8.entity;

/**
 *  entity of xml file which has some more entities inside
 */
public class GrowingTips {
    private Tempreture tempreture;
    private Lighting lighting;
    private Watering watering;


    public Tempreture getTempreture() {
        return tempreture;
    }

    public void setTempreture(Tempreture tempreture) {
        this.tempreture = tempreture;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }
}

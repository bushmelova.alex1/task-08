package com.epam.rd.java.basic.task8.entity;

/**
 * entity of the xml file which has some more entities inside
 */
public class VisualParameters {
    private String stemColor;
    private String leafColor;
    private AveLenFlower aveLenFlower;

    public String getStemColor() {
        return stemColor;
    }

    public void setStemColor(String stemColor) {
        this.stemColor = stemColor;
    }

    public String getLeafColor() {
        return leafColor;
    }

    public void setLeafColor(String leafColor) {
        this.leafColor = leafColor;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}

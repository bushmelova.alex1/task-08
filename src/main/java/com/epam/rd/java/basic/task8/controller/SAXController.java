package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constant.Constants;
import com.epam.rd.java.basic.task8.constant.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private final String xmlFileName;
    private Flowers flowers;
    private Flower flower;
    private VisualParameters visualParameters;
    private AveLenFlower aveLenFlower;
    private GrowingTips growingTips;
    private Tempreture tempreture;
    private Lighting lighting;
    private Watering watering;
    private String currentElement;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() {
        return flowers;
    }

    public void parseSAXFile(boolean parse) throws SAXException, ParserConfigurationException, IOException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setFeature(Constants.SET_FEATURE_DOCUMENT_BUILDER, true);
        saxParserFactory.setNamespaceAware(true);
        if (parse) {
            saxParserFactory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            saxParserFactory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }
        saxParserFactory.newSAXParser().parse(xmlFileName, handler);
    }

    /**
     * methods to establish all needed entities from the xml file
     */
    private final DefaultHandler handler = new DefaultHandler() {
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            currentElement = localName;
            if (XML.FLOWERS.getElement().equals(currentElement)) {
                flowers = new Flowers();
                return;
            }
            if (XML.FLOWER.getElement().equals(currentElement)) {
                flower = new Flower();
                return;
            }
            if (XML.VISUAL_PARAMETERS.getElement().equals(currentElement)) {
                visualParameters = new VisualParameters();
                return;
            }
            if (XML.AVE_LEN_FLOWER.getElement().equals(currentElement)) {
                aveLenFlower = new AveLenFlower();
                return;
            }
            if (XML.GROWING_TIPS.getElement().equals(currentElement)) {
                growingTips = new GrowingTips();
                return;
            }
            if (XML.TEMPRETURE.getElement().equals(currentElement)) {
                tempreture = new Tempreture();
                return;
            }
            if (XML.LIGHTING.getElement().equals(currentElement)) {
                lighting = new Lighting();
                lighting.setLightRequiring(attributes.getValue(XML.LIGHT_REQUIRING.getElement()));
                return;
            }
            if (XML.WATERING.getElement().equals(currentElement)) {
                watering = new Watering();
            }

        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if (XML.FLOWER.getElement().equals(localName)) {
                flowers.getFlowers().add(flower);
                return;
            }
            if (XML.VISUAL_PARAMETERS.getElement().equals(localName)) {
                flower.setVisualParameters(visualParameters);
                return;
            }
            if (XML.AVE_LEN_FLOWER.getElement().equals(localName)) {
                visualParameters.setAveLenFlower(aveLenFlower);
                return;
            }
            if (XML.GROWING_TIPS.getElement().equals(localName)) {
                flower.setGrowingTips(growingTips);
                return;
            }
            if (XML.TEMPRETURE.getElement().equals(localName)) {
                growingTips.setTempreture(tempreture);
                return;
            }
            if (XML.LIGHTING.getElement().equals(localName)) {
                growingTips.setLighting(lighting);
                return;
            }
            if (XML.WATERING.getElement().equals(localName)) {
                growingTips.setWatering(watering);
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) {
            String string = new String(ch, start, length);
            if (XML.NAME.getElement().equals(currentElement)) {
                flower.setName(string);
                return;
            }
            if (XML.SOIL.getElement().equals(currentElement)) {
                flower.setSoil(string);
                return;
            }
            if (XML.ORIGIN.getElement().equals(currentElement)) {
                flower.setOrigin(string);
                return;
            }
            if (XML.STEM_COLOUR.getElement().equals(currentElement)) {
                visualParameters.setStemColor(string);
                return;
            }
            if (XML.LEAF_COLOUR.getElement().equals(currentElement)) {
                visualParameters.setLeafColor(string);
                return;
            }
            if (XML.AVE_LEN_FLOWER.getElement().equals(currentElement)) {
                aveLenFlower.setAveLenFlower(Integer.parseInt(string));
                visualParameters.setAveLenFlower(aveLenFlower);
                return;
            }
            if (XML.TEMPRETURE.getElement().equals(currentElement)) {
                tempreture.setTempreture(Integer.parseInt(string));
                growingTips.setTempreture(tempreture);
                return;
            }
            if (XML.WATERING.getElement().equals(currentElement)) {
                watering.setWatering(Integer.parseInt(string));
                growingTips.setWatering(watering);
                return;
            }
            if (XML.MULTIPLYING.getElement().equals(currentElement)) {
                flower.setMultiplying(string);
            }

        }
    };
}
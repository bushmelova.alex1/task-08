package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.constant.Constants;
import com.epam.rd.java.basic.task8.constant.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	/**
	 * Parse the xml file.
	 * @param parse
	 * @return the container of all objects inside the file
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 */
    public Flowers parseXMLFile(boolean parse) throws ParserConfigurationException, IOException, SAXException {

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setFeature(Constants.SET_FEATURE_DOCUMENT_BUILDER,true);
		documentBuilderFactory.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING,true);
		documentBuilderFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD,Constants.EMPTY_STRING);
		documentBuilderFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA,Constants.EMPTY_STRING);
		documentBuilderFactory.setNamespaceAware(true);
		if (parse){
			documentBuilderFactory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON,false);
			documentBuilderFactory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON,false);
		}
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.parse(xmlFileName);
		Element rootElement = document.getDocumentElement();
		Flowers flowers = new Flowers();
		NodeList flowerList = rootElement.getElementsByTagName(XML.FLOWER.getElement());
		for (int i = 0; i < flowerList.getLength(); i++) {
			Flower flower = getFlowerElementFromXMLFile(flowerList.item(i));
			flowers.getFlowers().add(flower);
		}
		return flowers;
	}

	private Flower getFlowerElementFromXMLFile(Node flowerNode){
		Flower flower = new Flower();
		Element element = (Element) flowerNode;

		Node nodeElement = element.getElementsByTagName(XML.NAME.getElement()).item(0);
		flower.setName(nodeElement.getTextContent());

		nodeElement = element.getElementsByTagName(XML.SOIL.getElement()).item(0);
		flower.setSoil(nodeElement.getTextContent());

		nodeElement = element.getElementsByTagName(XML.ORIGIN.getElement()).item(0);
		flower.setOrigin(nodeElement.getTextContent());

		nodeElement = element.getElementsByTagName(XML.VISUAL_PARAMETERS.getElement()).item(0);
		flower.setVisualParameters(getVisualParameter(nodeElement));

		nodeElement = element.getElementsByTagName(XML.GROWING_TIPS.getElement()).item(0);
        flower.setGrowingTips(getGrowingTips(nodeElement));

		nodeElement = element.getElementsByTagName(XML.MULTIPLYING.getElement()).item(0);
		flower.setMultiplying(nodeElement.getTextContent());
		return flower;

	}
	public VisualParameters getVisualParameter(Node elementVisualParameter){
		VisualParameters visualParameters = new VisualParameters();
		AveLenFlower aveLenFlower = new AveLenFlower();
		Element element = (Element) elementVisualParameter;
		NodeList visualParameterNode = element.getChildNodes();
		for (int i = 0; i < visualParameterNode.getLength(); i++) {
			if(!visualParameterNode.item(i).getTextContent().trim().isEmpty()){
				if (visualParameterNode.item(i).getNodeName().equals(XML.STEM_COLOUR.getElement())){
					visualParameters.setStemColor(visualParameterNode.item(1).getTextContent());
				}else if (visualParameterNode.item(i).getNodeName().equals(XML.LEAF_COLOUR.getElement())){
					visualParameters.setLeafColor(visualParameterNode.item(1).getTextContent());
				}else{
					aveLenFlower.setAveLenFlower(Integer.parseInt(visualParameterNode.item(i).getTextContent()));
					visualParameters.setAveLenFlower(aveLenFlower);
				}
			}
		}
		return visualParameters;
	}

	public GrowingTips getGrowingTips(Node growingTipsNode){
		GrowingTips growingTips = new GrowingTips();
		Tempreture tempreture = new Tempreture();
		Lighting lighting = new Lighting();
		Watering watering = new Watering();
		Element element = (Element) growingTipsNode;
		NodeList nodeList = element.getChildNodes();
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i).getNodeName().equals(XML.TEMPRETURE.getElement())){
				tempreture.setTempreture(Integer.parseInt(nodeList.item(i).getTextContent()));
			}else if (nodeList.item(i).getNodeName().equals(XML.LIGHTING.getElement())){
				lighting.setLightRequiring(nodeList.item(i).getAttributes().item(0).getTextContent());
			}else if (nodeList.item(i).getNodeName().equals(XML.WATERING.getElement())){
				watering.setWatering(Integer.parseInt(nodeList.item(i).getTextContent()));
			}
		}
		growingTips.setTempreture(tempreture);
		growingTips.setLighting(lighting);
		growingTips.setWatering(watering);
		return growingTips;
	}

	/**
	 * Returns the whole xml document which was created
	 * @param flowers
	 * @return xml document
	 * @throws ParserConfigurationException
	 */

	public static Document createXMLDocument(Flowers flowers) throws ParserConfigurationException {
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		documentBuilderFactory.setFeature(Constants.SET_FEATURE_DOCUMENT_BUILDER,true);
		documentBuilderFactory.setNamespaceAware(true);

		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.newDocument();

		Element elementOutput = document.createElement(XML.FLOWERS.getElement());
		elementOutput.setAttribute(Constants.SET_XMLNS,Constants.SET_XMLNS_SITE);
		elementOutput.setAttribute(Constants.SET_XSI,Constants.SET_XSI_SITE);
		elementOutput.setAttribute(Constants.SET_SCHEMA_LOCATION,Constants.SET_SCHEMA_LOCATION_SITE);
		document.appendChild(elementOutput);

		for (Flower flower :flowers.getFlowers()) {

			Element elementFlower = document.createElement(XML.FLOWER.getElement());
			Element element = document.createElement(XML.NAME.getElement());
			element.setTextContent(flower.getName());
			elementFlower.appendChild(element);
			element = document.createElement(XML.SOIL.getElement());
			element.setTextContent(flower.getSoil());
			elementFlower.appendChild(element);
			element = document.createElement(XML.ORIGIN.getElement());
			element.setTextContent(flower.getOrigin());
			elementFlower.appendChild(element);

			element = document.createElement(XML.VISUAL_PARAMETERS.getElement());
			elementFlower.appendChild(element);

			Element innerElement = document.createElement(XML.STEM_COLOUR.getElement());
			innerElement.setTextContent(flower.getVisualParameters().getStemColor());
			element.appendChild(innerElement);

			innerElement = document.createElement(XML.LEAF_COLOUR.getElement());
			innerElement.setTextContent(flower.getVisualParameters().getLeafColor());
			element.appendChild(innerElement);

			Element doubleInnerElement =document.createElement(XML.AVE_LEN_FLOWER.getElement());
			doubleInnerElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getAveLenFlower().toString());
			doubleInnerElement.setAttribute(XML.MEASURE.getElement(), XML.CM.getElement());
			element.appendChild(doubleInnerElement);

			element = document.createElement(XML.GROWING_TIPS.getElement());
			elementFlower.appendChild(element);

			innerElement = document.createElement(XML.TEMPRETURE.getElement());
			innerElement.setTextContent(String.valueOf(flower.getGrowingTips().getTempreture()));
			innerElement.setAttribute(XML.MEASURE.getElement(), XML.CELCIUS.getElement());
			element.appendChild(innerElement);

			innerElement = document.createElement(XML.LIGHTING.getElement());
			innerElement.setAttribute(XML.LIGHT_REQUIRING.getElement(),flower.getGrowingTips().getLighting().getLightRequiring());
			element.appendChild(innerElement);

			innerElement = document.createElement(XML.WATERING.getElement());
			innerElement.setTextContent(String.valueOf(flower.getGrowingTips().getWatering().getWatering()));
			innerElement.setAttribute(XML.MEASURE.getElement(), XML.ML_PER_WEEK.getElement());
			element.appendChild(innerElement);

			element = document.createElement(XML.MULTIPLYING.getElement());
			element.setTextContent(flower.getMultiplying());
			elementFlower.appendChild(element);

			elementOutput.appendChild(elementFlower);

		}

		return document;
	}

	public static void saveToXMLFile(Flowers flowers,String fileName) throws TransformerException, ParserConfigurationException {

		StreamResult result = new StreamResult(new File(fileName));
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING,true);
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD,Constants.EMPTY_STRING);
		transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET,Constants.EMPTY_STRING);
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT,XML.YES.getElement());
		transformer.transform(new DOMSource(createXMLDocument(flowers)),result);
	}



}

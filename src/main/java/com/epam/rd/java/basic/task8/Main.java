package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.util.Sort;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		Flowers flowers = domController.parseXMLFile(true);

		// sort (case 1)
          Sort.sortCaseOne(flowers);
		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXMLFile(flowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parseSAXFile(true);
		Flowers flowers2 = saxController.getFlowers();
		// sort  (case 2)
		Flowers sortedSAXFlowers = Sort.sortCaseTwo(flowers2);
		// save
		outputXmlFile = "output.sax.xml";
        DOMController.saveToXMLFile(sortedSAXFlowers,outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parseXMLFileSTAX();
		Flowers flowers3 = staxController.getFlowers();
		staxController.print();
		
		// sort  (case 3)
		Flowers sortedStaxFlowers = Sort.sortCaseThree(flowers3);
		
		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveToXMLFile(sortedStaxFlowers,outputXmlFile);
	}

}

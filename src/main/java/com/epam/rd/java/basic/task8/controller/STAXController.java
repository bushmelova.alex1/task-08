package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.constant.XML;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private final String xmlFileName;
    List<Flower> flowersList = new ArrayList<>();
    Flowers flowers = new Flowers();
    Flower flower = new Flower();
    VisualParameters visualParameters = new VisualParameters();
    AveLenFlower aveLenFlower = new AveLenFlower();
    GrowingTips growingTips = new GrowingTips();
    Tempreture tempreture = new Tempreture();
    Lighting lighting = new Lighting();
    Watering watering = new Watering();
    String currentElement = null;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public Flowers getFlowers() {
        return flowers;
    }
    public  void print(){
        System.out.println(flowersList);
    }

    /**
     * method parse xml file and prepare entities to be written to the stax document file
     * @throws XMLStreamException
     */
    public void parseXMLFileSTAX() throws XMLStreamException {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
        XMLEventReader eventReader = xmlInputFactory.createXMLEventReader(new StreamSource(xmlFileName));


        while (eventReader.hasNext()) {
            XMLEvent nextEvent = eventReader.nextEvent();

            if (nextEvent.isCharacters() && nextEvent.asCharacters().isWhiteSpace()) {
                continue;
            }
            if (nextEvent.isStartElement()) {
                StartElement startElement = nextEvent.asStartElement();
                currentElement = startElement.getName().getLocalPart();


                if (currentElement.equals(XML.FLOWER.getElement())) {
                    flower = new Flower();
                }
                if (currentElement.equals(XML.VISUAL_PARAMETERS.getElement())) {
                    visualParameters = new VisualParameters();

                }
                if (currentElement.equals(XML.AVE_LEN_FLOWER.getElement())) {
                    aveLenFlower = new AveLenFlower();
                }
                if (currentElement.equals(XML.GROWING_TIPS.getElement())) {
                    growingTips = new GrowingTips();
                }
                if (currentElement.equals(XML.TEMPRETURE.getElement())) {
                    tempreture = new Tempreture();
                }
                if (currentElement.equals(XML.LIGHTING.getElement())) {
                    lighting = new Lighting();
                    Iterator<Attribute> attributes = startElement.getAttributes();
                    lighting.setLightRequiring(attributes.next().getValue());

                }
                if (currentElement.equals(XML.WATERING.getElement())) {
                    watering = new Watering();
                }


            }


            if (nextEvent.isCharacters()) {
                Characters chars = nextEvent.asCharacters();
                if (XML.NAME.getElement().equals(currentElement)) {
                    flower.setName(chars.getData());
                }
                if (XML.SOIL.getElement().equals(currentElement)) {
                    flower.setSoil(chars.getData());
                }
                if (XML.ORIGIN.getElement().equals(currentElement)) {
                    flower.setOrigin(chars.getData());
                }
                if (XML.STEM_COLOUR.getElement().equals(currentElement)) {
                    visualParameters.setStemColor(chars.getData());
                }
                if (XML.LEAF_COLOUR.getElement().equals(currentElement)) {
                    visualParameters.setLeafColor(chars.getData());
                }
                if (XML.AVE_LEN_FLOWER.getElement().equals(currentElement)) {
                    aveLenFlower.setAveLenFlower(Integer.parseInt(chars.getData()));
                    visualParameters.setAveLenFlower(aveLenFlower);
                }
                if (XML.TEMPRETURE.getElement().equals(currentElement)) {
                    tempreture.setTempreture(Integer.parseInt(chars.getData()));
                    growingTips.setTempreture(tempreture);
                }
                if (XML.LIGHTING.getElement().equals(currentElement)) {
                    lighting.setLightRequiring(chars.getData());
                    growingTips.setLighting(lighting);
                }
                if (XML.WATERING.getElement().equals(currentElement)) {
                    watering.setWatering(Integer.parseInt(chars.getData()));
                    growingTips.setWatering(watering);
                }
                if (XML.MULTIPLYING.getElement().equals(currentElement)) {
                    flower.setMultiplying(chars.getData());
                }
            }


            if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                String currElement = endElement.getName().getLocalPart();

                if (currElement.equals(XML.FLOWER.getElement())) {
                    flowersList.add(flower);
                }
                if (currElement.equals(XML.VISUAL_PARAMETERS.getElement())) {
                    assert flower != null;
                    flower.setVisualParameters(visualParameters);
                }
                if (currElement.equals(XML.AVE_LEN_FLOWER.getElement())) {
                    visualParameters.setAveLenFlower(aveLenFlower);
                }
                if (currElement.equals(XML.GROWING_TIPS.getElement())) {
                    flower.setGrowingTips(growingTips);
                }
                if (currElement.equals(XML.TEMPRETURE.getElement())) {
                    growingTips.setTempreture(tempreture);
                }
                if (currElement.equals(XML.LIGHTING.getElement())) {
                    growingTips.setLighting(lighting);
                }
                if (currElement.equals(XML.WATERING.getElement())) {
                    growingTips.setWatering(watering);
                }
                if (currElement.equals(XML.FLOWERS.getElement())) {
                    flowers.setFlowers(flowersList);
                }
            }

        }
        eventReader.close();
        flowers.setFlowers(flowersList);
    }
}
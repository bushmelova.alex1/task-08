package com.epam.rd.java.basic.task8.constant;

/**
 * Constants of xml file main elements and attributes
 */
public enum XML {
    FLOWERS("flowers"), FLOWER("flower"), NAME("name"),
    SOIL("soil"), ORIGIN("origin"), VISUAL_PARAMETERS("visualParameters"),
    STEM_COLOUR("stemColour"), LEAF_COLOUR("leafColour"), AVE_LEN_FLOWER("aveLenFlower"),
    GROWING_TIPS("growingTips"), TEMPRETURE("tempreture"), LIGHTING("lighting"),
    WATERING("watering"), MEASURE("measure"), MULTIPLYING("multiplying"),
    CELCIUS("celcius"),CM("cm"),LIGHT_REQUIRING("lightRequiring"),YES("yes"),
    ML_PER_WEEK("mlPerWeek");

    private final String element;

    XML(String element) {
        this.element = element;
    }

    public String getElement() {
        return element;
    }
}

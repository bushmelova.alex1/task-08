package com.epam.rd.java.basic.task8.constant;

/**
 * Constants for the validation features and additional constants
 */
public class Constants {
    private Constants() {
    }

    public static final String FEATURE_TURN_VALIDATION_ON = "http://xml.org/sax/features/validation";
    public static final String FEATURE_TURN_SCHEMA_VALIDATION_ON = "http://apache.org/xml/features/validation/schema";
    public static final String SET_FEATURE_DOCUMENT_BUILDER = "http://apache.org/xml/features/disallow-doctype-decl";
    public static final String SET_XMLNS = "xmlns";
    public static final String SET_XSI ="xmlns:xsi";
    public static final String SET_SCHEMA_LOCATION="xsi:schemaLocation";
    public static final String SET_XMLNS_SITE ="http://www.nure.ua";
    public static final String SET_XSI_SITE="http://www.w3.org/2001/XMLSchema-instance";
    public static final String SET_SCHEMA_LOCATION_SITE="http://www.nure.ua input.xsd ";
    public static final String EMPTY_STRING = "";
}

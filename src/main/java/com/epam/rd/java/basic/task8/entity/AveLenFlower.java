package com.epam.rd.java.basic.task8.entity;

/**
 * entity of the xml file
 */
public class AveLenFlower {
    private Integer aveLenFlower;

    public Integer getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(Integer aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}

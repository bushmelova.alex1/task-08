package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * container of all entities which are in the xml file
 */
public class Flowers {
    private List<Flower> flowers;

    public List<Flower> getFlowers() {
        if (flowers == null) {
            flowers = new ArrayList<>();
        }
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }
}

package com.epam.rd.java.basic.task8.entity;

/**
 * simple entity of the xml file
 */
public class Watering {
    private Integer watering;


    public Integer getWatering() {
        return watering;
    }

    public void setWatering(Integer watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return watering.toString();
    }
}
